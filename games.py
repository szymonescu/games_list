import json
from operator import itemgetter

from utils import get_file_contents


class GamesResource:
    def __init__(self, filters, filename):
        self.filters = filters
        self.filename = filename

    def on_get(self, req, resp):
        data = get_file_contents(self.filename)
        params = req.params

        for filter_ in self.filters:
            data = filter_.filter(data, params)

        sort_by = params.get('sort_by')
        if sort_by is not None:
            data = sorted(data, key=itemgetter(sort_by))

        resp.body = json.dumps(data)
