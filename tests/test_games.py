import os
from operator import itemgetter

import pytest
from falcon import testing, falcon

from app import filters
from games import GamesResource
from utils import get_file_contents


@pytest.fixture(scope='module')
def client():
    filename = os.path.join(os.path.dirname(__file__), 'test_games_list.json')
    games = GamesResource(filters, filename)
    app = falcon.API()
    app.add_route('/games', games)
    return testing.TestClient(app)


@pytest.fixture
def file_contents():
    return get_file_contents(
        os.path.join(os.path.dirname(__file__), 'test_games_list.json'))


class TestGamesResourceOnGet:
    def test_returns_same_list_with_no_sorting_no_filtering_when_no_qs(
            self, client, file_contents):
        result = client.simulate_get('/games')
        assert result.json == file_contents

    def test_returns_results_filtered_by_name(self, client):
        result = client.simulate_get(
            '/games', params={'name': 'Pomodoro'})
        assert result.json == [
            {
                "name": "Pomodoro",
                "publisher": "Pommodorro",
                "genre": "RPG"
            },
            {
                "name": "Pomodoro",
                "publisher": "Nju pabliszer",
                "genre": "MMO"
            }
        ]

    def test_returns_results_filtered_by_publisher(self, client):
        result = client.simulate_get(
            '/games', params={'publisher': 'Pommodorro'})
        assert result.json == [
            {
                "name": "Pomodoro",
                "publisher": "Pommodorro",
                "genre": "RPG"
            },
            {
                "name": "Nju Gejm",
                "publisher": "Pommodorro",
                "genre": "RPG"
            }
        ]

    def test_returns_results_filtered_by_genre(self, client):
        result = client.simulate_get(
            '/games', params={'genre': 'MMO'})
        assert result.json == [
            {
                "name": "Pomodoro",
                "publisher": "Nju pabliszer",
                "genre": "MMO"
            }
        ]

    @pytest.mark.parametrize(
        'sort_by',
        [
            'name',
            'publisher',
            'genre'
        ]
    )
    def test_returns_results_sorted_by_param(
            self, sort_by, client, file_contents):
        sorted_contents = sorted(file_contents, key=itemgetter(sort_by))
        result = client.simulate_get(
            '/games', params={'sort_by': sort_by})
        assert result.json == sorted_contents
