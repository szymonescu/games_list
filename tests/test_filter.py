import pytest

from filter import Filter


@pytest.fixture
def some_filter():
    return Filter('some')


class TestFilter:
    def test_returns_filtered_results(self, some_filter):
        data = [
            {'some': 'nothing'},
            {'some': 'something'}
        ]
        params = {'some': 'nothing'}
        result = some_filter.filter(data, params)
        assert result == [{'some': 'nothing'}]

    def test_returns_input_data_when_no_filter_value(self, some_filter):
        data = [
            {'some': 'nothing'},
            {'some': 'something'}
        ]
        params = {'some_other': 'nothing'}
        result = some_filter.filter(data, params)
        assert result == data
