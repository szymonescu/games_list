# Summary
Simple api to filter and/or sort results of request. Any error handling or validation was omitted, but it could be added
in the future. The file that the data is being read from could also be made configurable upon launching the app.

## Running the server
```bash
git clone git@bitbucket.org:szymonescu/games_list.git
cd games_list
pip install -r requirements.txt
. run.sh
```

## Tests
```bash
git clone git@bitbucket.org:szymonescu/games_list.git
cd games_list
pip install -r requirements_tests.txt
py.test tests
```

# Sample usages
## filter by name, publisher or genre
```bash 
curl http://127.0.0.1:8000/games?name=The%20Game
curl http://127.0.0.1:8000/games?publisher=The%20publisher
curl http://127.0.0.1:8000/games?genre=action
```

## filter by two+ criteria
```bash 
curl http://127.0.0.1:8000/games?genre=action&publisher=Banananana
curl http://127.0.0.1:8000/games?name=Banana&genre=adventure
```

## sort by name, publisher or genre, can be combined with filtering
:exclamation: note that sorting can only take one param
```bash
curl http://127.0.0.1:8000/games?sort_by=name
curl http://127.0.0.1:8000/games?sort_by=publisher
curl http://127.0.0.1:8000/games?sort_by=genre
```
