import json


def get_file_contents(filename):
    with open(filename) as f:
        contents = json.load(f)
    return contents['games']
