class Filter:
    def __init__(self, name):
        self.name = name

    def filter(self, data, params):
        value = params.get(self.name)
        if value is not None:
            return  list(filter(lambda row: row[self.name] == value, data))
        else:
            return data
