import os

import falcon


from filter import Filter
from games import GamesResource

name_filter = Filter('name')
genre_filter = Filter('genre')
publisher_filter = Filter('publisher')

filters = [
    name_filter,
    genre_filter,
    publisher_filter,
]

filename = os.path.join(os.path.dirname(__file__), 'GamesList.json')

games = GamesResource(filters, filename)
app = falcon.API()

app.add_route('/games', games)
